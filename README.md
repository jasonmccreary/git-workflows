# Git Workflows Exercise
This exercise is broken into two parts to help practice the Git commands for sharing work with a _remote_ respository, including: `branch`, `checkout`, `merge`, `rebase`, `clone`, `push`, `pull`, and `fetch`.

## Part 1
For this exercise, create a local copy of [the training repository](https://bitbucket.org/medtrainerdevelopment/git-training) and on a separate branch add a simple text bio with your name and **two Git questions** you would like answered in tomorrow's training.

For this exercise, complete the following steps. **Don't forget to track each Git command you run** in the `command.log`. For this exercise, prefix your `command.log` with your name. For example, `jmac-command.log`.

1. Clone this repository locally.
2. Create a new branch off `master` of your first name and prefixed with `feature-`. For example, `feature-jmac`.
3. Add a text file to the `bios` folder with your first name. For example, `jmac.txt`.
4. Commit your changes (don't forget to update your `command.log`)
5. Push your changes to the remote.
6. Verify your branch exists on Bitbucket.
7. Update your bio with your preferred name and two Git questions.
8. Commit your changes (don't forget to update your `command.log`)
9. Push your changes to the remote.
10. Open a _Pull Request_ to merge your branch with the `master` branch.
11. Let _JMac_ know you have created the pull request.
12. Locally, checkout the `master` branch.
13. View the commit history for all branches.
14. Fetch the latest changes from the remote repository.
15. Update your branch with the latest changes.
16. Push your changes to the remote.
17. Verify your Pull Request only contains your changes.


## Part 2
For this exercise, team up with a partner and create a new branch to share work between each other and create a single _Pull Request_ for that work.

For this exercise, complete the following steps. **Don't forget to track each Git command you run** in your `command.log`.

1. Create a new branch off `master` with a unique name.
2. **Member 1**:
    1. Add a new text file called `pairing.txt`.
    2. Commit your changes (don't forget to update the `command.log`)
    3. Push your changes to the remote.
3. ** Member 2**:
    1. Checkout the new branch locally.
    2. Update the `pairing.txt` file with a team name.
	3. Commit your changes 
    4. Push your changes to the remote.
4. **Member 1** pull down the lastest changes.
5. **Both members**:
    1. Append a timestamp to `pairing.txt`.
    2. Commit your changes.
	3. Push your changes to the remote. (which one failed?)
7. **Working together:**
	1. Update the failed branch with the latest changes.
	2. Push your changes to the remote.
	3. Open a _Pull Request_ to merge your branch with the `master` branch.
